import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import {Observable} from 'rxjs/Observable';

/*
  Generated class for the Categorias page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html'
})
export class CategoriasPage {
    
    notesSoc: FirebaseListObservable<any>;
     notesDep: FirebaseListObservable<any>;
     notesCie: FirebaseListObservable<any>;
     notesEsp: FirebaseListObservable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFire) {
      
      this.notesSoc = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: "Sociales"
            }}); 
      this.notesDep = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: "Deportes"
            }}); 
      this.notesCie = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: "Ciencia"
            }}); 
      this.notesEsp = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: "Espectaculos"
            }}); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriasPage');
  }

}
