import { Component } from '@angular/core';
import { NavController, AlertController, ActionSheetController, NavParams   } from 'ionic-angular';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { AuthPage } from '../auth/home/home';
import {Observable} from 'rxjs/Observable';
import { DataProvider } from '../../providers/data';
import firebase from 'firebase';
import {AuthProvider} from '../../providers/auth';

@Component({
  templateUrl: 'home.html',
  selector: 'home',
})
export class HomePage {
    
    user: FirebaseListObservable<any>;
    noteRef: any;
    noteList: any;
    loadedNotesList: any;
    modifiedData: any;
    notes: FirebaseListObservable<any>;
    notesCat: FirebaseListObservable<any>;
    userDat: any;
    parameter1: any;
    parameter2: any;
    showAddBtn = true;
    showEditBtn = true;
    
   
    
 
    
  constructor( public alertCtrl: AlertController, private af: AngularFire, public actionSheetCtrl: ActionSheetController, public dataService: DataProvider, public auth: AuthProvider, private navParams: NavParams, public navCtrl: NavController, private data: DataProvider) {
      
    this.parameter1 = navParams.get('response'); 
    this.parameter2 = navParams.get('feisbuk'); 
      
    this.notes = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: "general"
          }}); 
      this.showAddBtn = false;
      this.showEditBtn = false;
 
      
      
if (this.parameter2 != null){
        this.user = af.database.list('/users', { query: { orderByChild: 'email',
            equalTo: this.parameter2.email
            }}); 
    console.log("Entra a parametro 2");
        if (this.parameter2.permiso == "a"){
            this.notes = af.database.list('/notes'); 
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: this.parameter2.categoria
            }}); 
            
            this.showAddBtn = true;
            this.showEditBtn = true;
        }else if (this.parameter2.permiso == "b"){
    
            this.notes = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: this.parameter2.permiso
          }}); 
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: this.parameter2.categoria
          }}); 
            
            
           
            this.showAddBtn = true;
            this.showEditBtn = false;
        }else if (this.parameter2.permiso == "c"){
            this.showAddBtn = false;
            this.showEditBtn = false;
            this.notes = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: this.parameter2.permiso
          }}); 
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: this.parameter2.categoria
          }}); 
            
    
            /*this.notesCat = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: this.parameter2.permiso
            }}, { query: { orderByChild: 'categoria',
            equalTo: this.parameter2.categoria
            }}); */
        }else{
             this.navCtrl.push(AuthPage);
        }
   
          
}else if (this.parameter1 != null){
    
        console.log("Entra a parametro 1");
        
        this.af.database.list('/users',{ query: { orderByChild: 'email',
        equalTo: this.parameter1.auth.displayName
    
        }}).subscribe(userData => {
            
            console.log(userData);
            this.user = af.database.list('/users', { query: { orderByChild: 'email',
equalTo: userData[0].email
            }}); 
            
        if (userData[0].permiso == "a"){
this.notes = af.database.list('/notes');
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: userData[0].categoria
          }}); 
            this.showAddBtn = true;
            this.showEditBtn = true;
        }else if (userData[0].permiso == "b"){
    
            this.notes = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: userData[0].permiso
          }}); 
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: userData[0].categoria
          }}); 
            this.showAddBtn = true;
            this.showEditBtn = false;
        }else if (userData[0].permiso == "c"){
            this.notes = af.database.list('/notes', { query: { orderByChild: 'id',
            equalTo: userData[0].permiso
          }}); 
            this.notesCat = af.database.list('/notes', { query: { orderByChild: 'categoria',
            equalTo: userData[0].categoria
          }}); 
    
            this.showAddBtn = false;
            this.showEditBtn = false;
        }else{
             this.navCtrl.push(AuthPage);
        }
          });
}
         
  }


    
    //Agregar notas
    
LogOut(){
    this.parameter1 = null;
    this.parameter2=null;
    this.navCtrl.push(AuthPage);
}


initializeItems(): void {
  this.noteList = this.loadedNotesList;
    
}
    addNote(){
  let prompt = this.alertCtrl.create({
    title: 'Titulo de la nota',
    message: "Ingresa un titulo para la nota",
    inputs: [
      {
        name: 'title',
        placeholder: 'Titulo'
          
        },{
            name: 'note',
            placeholder:'Nota'
        },
        
    ],
    buttons: [
      {
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.notes.push({
            title: data.title,
              note: data.note,
              id: "rev"
          });
        }
      }
    ]
  });
  prompt.present();
}
    //termina agregar notas
    
    //Inicia eliminar notas
    removeNote(noteId: string){
  this.notes.remove(noteId);
}
    
    //Termina eliminar notas
    
    //Inicia actualizar notas
    updateNote(noteId, noteTitle,note,id,categoria){
  let prompt = this.alertCtrl.create({
    title: 'Titulo de la nota',
    message: "Actualiza el nombre de esta nota",
    inputs: [
      {
        name: 'title',
        placeholder: 'Title',
        value: noteTitle
      },{
        name: 'note',
        placeholder: 'Nota',
        value: note
      },
        {
        name: 'id',
        placeholder: 'Permiso necesario',
        value: id
      },
        {
        name: 'categoria',
        placeholder: 'Categoria',
        value: categoria
      }
    ],
    buttons: [
      {
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.notes.update(noteId, {
            title: data.title,
            note: data.note,
            id: data.id,
              categoria: data.categoria
          });
        }
      }
    ]
  });
  prompt.present();
}
    
    //Termina Actualizar notas
    
    
    updateUser(userId, categoria){
  let prompt = this.alertCtrl.create({
    title: 'Configuraciones de usuario',
    message: "Actualiza tu categoria favorita",
    inputs: [
      {
        name: 'categoria',
        placeholder: 'Categoria',
        value: categoria
      }
    ],
    buttons: [
      {
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.user.update(userId, {
            categoria: data.categoria

            
            });
        }
      }
    ]
  });
  prompt.present();
}
    
    
    //Inicia mostrar opciones
    showOptions(noteId, noteTitle,note,id, categoria) {
  let actionSheet = this.actionSheetCtrl.create({
    title: '¿Que quieres hacer?',
    buttons: [
      {
        text: 'Eliminar nota',
        role: 'destructive',
        handler: () => {
          this.removeNote(noteId);
        }
      },{
        text: 'Actualizar entrada',
        handler: () => {
          this.updateNote(noteId, noteTitle, note, id, categoria);
        }
      },{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  actionSheet.present();
}
    //Termina mostrar opciones

  ngOnInit() {
    
  }



  
}
