import { NavController, Platform } from 'ionic-angular';
import { Component ,Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { LoginEmailPage } from '../login-email/login-email';
import { SignUpPage } from '../sign-up/sign-up';
import { TermsOfServicePage } from '../../terms-of-service/terms-of-service';
import { AuthProvider } from '../../../providers/auth';
import {AngularFire, AuthProviders, AuthMethods} from 'angularfire2';

import { HomePage } from '../../home/home';

@Component({
  templateUrl: 'home.html',
  selector: 'auth-home',
})

export class AuthPage {
  error: any;

  constructor(private navCtrl: NavController, private auth: AuthProvider, public angfire: AngularFire, private platform: Platform) {
      

  }

  ngOnInit() {

  }

  openSignUpPage() {
    this.navCtrl.push(SignUpPage);
  }

  openLoginPage() {
    this.navCtrl.push(LoginEmailPage);
  }

  openTermsOfService() {
    this.navCtrl.push(TermsOfServicePage);
  }

  loginUserWithFacebook() {
    this.auth.loginWithFacebook().subscribe(data => {
      //this.navCtrl.setRoot(HomePage);
    
        
    }, err => {
      this.error = err;
    });
      
      this.navCtrl.setRoot(HomePage, {
          feisbuk: this.auth.user
        });
  }
    
    twitterlogin() {
    this.angfire.auth.login({
      provider: AuthProviders.Twitter,
      method: AuthMethods.Popup
    }).then((response) => {
        this.angfire.database.list('users').update(response.uid, {
              name: response.auth.displayName,
              email: response.auth.displayName,
              provider: 'twitter',
              image: response.auth.photoURL
            });
      //console.log('Login success with twitter' + JSON.stringify(response));
        this.navCtrl.setRoot(HomePage, {
            response
        });
      let currentuser = {
          email: response.auth.displayName,
          picture: response.auth.photoURL
        };
        window.localStorage.setItem('currentuser', JSON.stringify(currentuser));
        //this.navCtrl.pop();
      }).catch((error) => {
        console.log(error);
    })
    
  }
    
    

}
